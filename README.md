# BashDocGenerator
 Small bash markdown documentation generation for bash for my libraries.

## Usage

```bash
./generator.py file.sh > output.md
```

## Example

### Bash source code:
```bash
#                                                                            
# BashOOP - Simple OOP implementation for bash.                              
# Copyright (C) 2022  Ad5001 <mail@ad5001.eu>                                
#                                                                            
# This program is free software: you can redistribute it and/or modify       
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or          
# (at your option) any later version.                                        
#                                                                            
# This program is distributed in the hope that it will be useful,            
# but WITHOUT ANY WARRANTY; without even the implied warranty of             
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
# GNU General Public License for more details.                               
#                                                                            
# You should have received a copy of the GNU General Public License          
# along with this program.  If not, see <http://www.gnu.org/licenses/>.      
#                                                                            

# Namespace related variables. Due to beginning with a _, it's considered internal.
_namespace=""

# Example test variable.
exampleVariable="test"                                                       

# Other variable
# Type: int
exampleVariable2=0                                                      

# Declares the current namespace.
# Signature: ([string namespaceName]) -> void                                
namespace() {                                                                
    _namespace=$1;                                                           
}                                                                           
                                                                             
# Imports a namespace into the current shell.                                
# It saves the path of the file so that relative paths can be                
# properly resolved.                                                         
# For example, if the object Object exists within namespace Example, it      
# will be accessible with "Example.Object".                                  
# Signature: (<string namespaceFile>) -> void                                
importNamespace() {                                                          
    namespaceFile=$1                                                         
    # Save the path in order to get the absolute path of the file.           
    _namespacePath=$(realpath $(dirname $namespaceFile))                     
    . $namespaceFile                                                         
}                                                                            
```

# Markdown equivalent

```markdown

# BashOOP Library Reference (oop.sh)
*Simple OOP implementation for bash.*    
Under [LGPLv3](https://www.gnu.org/licenses/lgpl-3.0.en.html) - Copyright (C) 2022  Ad5001 <mail@ad5001.eu> 

---

|Contents                          |
|----------------------------------|
|<ul>[Variables](#variables)</ul>  |
|<ul>[Classes](#classes)</ul>      |
|<ul>[Properties](#properties)</ul>|
|<ul>[Methods](#methods)</ul>      |

<br><br>
    
## Variables

### Reference

- `string  ` [`exampleVariable`](#var-exampleVariable)
- `int  ` [`exampleVariable2`](#var-exampleVariable2)

### Detailed Documentation

<ul id="var-exampleVariable"><li><code>string exampleVariable</code></li></ul>

Example test variable.

<ul id="var-exampleVariable"><li><code>int exampleVariable2</code></li></ul>

Other variable.


## Methods
**Note**: Arguments between <> are considered mandatory, while the ones within [] are considered optionnal.

### Reference

- [`namespace`](#method-namespace)` [string namespaceName] → void`
- [`importNamespace`](#method-importNamespace)` <string namespaceFile> → void` 

### Detailed Documentation

<ul id="method-namespace"><li><code>namespace [string namespaceName] → void</code></li></ul>
Declares the current namespace.

<ul id="method-importNamespace"><li><code>importNamespace &lt;string namespaceFile&gt; → void</code></li></ul>
Imports a namespace into the current shell.
It saves the path of the file so that relative paths can be properly resolved.                                                         
For example, if the object Object exists within namespace Example, it will be accessible with "Example.Object".
```
